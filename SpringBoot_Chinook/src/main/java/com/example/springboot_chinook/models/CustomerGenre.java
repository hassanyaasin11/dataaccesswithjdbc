package com.example.springboot_chinook.models;

/**
 * The CustomerGenre model will be used in the mostPopularGenre(int id) function to be returned as an object including
 * the name of the most popular genre together with additional information such as the id of the (genre_id) and the
 * full name of the given customer. This function will show what genre is most popular for the given customer in customer_id.
 * @param name
 * @param id
 * @param firstName
 * @param lastName
 */
public record CustomerGenre(String name, int id, String firstName, String lastName) {
}
