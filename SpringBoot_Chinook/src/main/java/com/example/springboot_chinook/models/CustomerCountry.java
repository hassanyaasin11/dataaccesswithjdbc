package com.example.springboot_chinook.models;

/**
 * This is the CustomerCountry model, this will be a record of a CustomerCountry object
 * especially used for the countryWithMostCustomers() function when returning the country that
 * has the most customers.
 * @param country
 */
public record CustomerCountry(String country) {
}
