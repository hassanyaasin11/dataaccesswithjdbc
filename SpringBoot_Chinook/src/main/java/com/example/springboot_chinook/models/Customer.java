package com.example.springboot_chinook.models;

/**
 * This is the Customer model, this will be a record of a Customer object where columns
 * of the "customer" table will be stored to the parameters in the following
 * order (customer_id, first_name, last_name, country, postal_code, phoneNumber, email).
 * @param id
 * @param firstName
 * @param lastName
 * @param country
 * @param postalCode
 * @param phoneNumber
 * @param email
 */
//Constructor
public record Customer (int id, String firstName, String lastName, String country,
                        String postalCode,String phoneNumber,String email) {
}
