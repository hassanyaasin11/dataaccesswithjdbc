package com.example.springboot_chinook.models;

/**
 * This is the CustomerSpender model, this will be a record of a CustomerSpender object returning
 * customer_id, first_name and last_name from customer table while returning total from invoice table
 * of the customer who spends the most. This model is used in used the highestSpender() function
 * to find out what customer spends most in the invoice table.
 * @param id
 * @param firstName
 * @param lastName
 * @param totalInvoice
 */
public record CustomerSpender (int id, String firstName, String lastName, double totalInvoice){
}
