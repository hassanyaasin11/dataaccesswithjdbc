package com.example.springboot_chinook.runners;

import com.example.springboot_chinook.models.Customer;
import com.example.springboot_chinook.repository.customer.CustomerRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.sql.SQLOutput;

/**
 * This is the CustomerRunner, a Spring Boot CommandLineRunner implementing the ApplicationRunner.
 * The CustomerRunner has a run method that will be implemented after the application context has been initialized.
 */
@Component
public class CustomerRunner implements ApplicationRunner {
    private final CustomerRepository customerRepository;

    public CustomerRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    /**
     * All the functions are being tested in this run() method below, the testing are listed below as according to the assignment tasks in order.
     * @param args
     * @throws Exception
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {

        // 1 Read all the customers in the database, this should display their: Id, first name, last name, country, postal code,
        //phone number and email.
        System.out.println(customerRepository.findAll());

        // 2 Read a specific customer from the database (by Id), should display everything listed in the above point.
        System.out.println(customerRepository.findById(2));

        // 3 Read a specific customer by name. HINT: LIKE keyword can help for partial matches.
        System.out.println(customerRepository.findByName("Daan"));

        // 4  Return a page of customers from the database. This should take in limit and offset as parameters and make use
        //of the SQL limit and offset keywords to get a subset of the customer data. The customer model from above
        //should be reused.
        System.out.println(customerRepository.findWithLimitAndOffset(5,1));

        // 5 Add a new customer to the database. You also need to add only the fields listed above (our customer object)
        System.out.println(customerRepository.insert((new Customer(60,"hae","ld","la","7621872","02901292918","ksakas"))));

        // 6. Update an existing customer.
        System.out.println(customerRepository.update(new Customer(1,"Husow","yeees","la","999999999","888888888888","ksakas")));

        // 7 Return the country with the most customers.
        System.out.println(customerRepository.countryWithMostCustomers());

        // 8 Customer who is the highest spender (total in invoice table is the largest).
        System.out.println(customerRepository.highestSpender());

        // 9 The most popular genre among the customers
        System.out.println(customerRepository.mostPopularGenre(12));
    }
}
