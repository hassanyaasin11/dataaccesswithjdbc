package com.example.springboot_chinook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootChinookApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootChinookApplication.class, args);
    }
//test
}
