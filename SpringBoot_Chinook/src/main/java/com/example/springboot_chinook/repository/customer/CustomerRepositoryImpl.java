package com.example.springboot_chinook.repository.customer;

import com.example.springboot_chinook.models.*;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The CustomerRepositoryImpl class will be the implementation of the repository.
 * All the methods are overridden from the repositories and the all the logic of the functions are coded in this class.
 */
@Repository
public class CustomerRepositoryImpl implements CustomerRepository{

    private String url;
    private String username;
    private String password;

    /**
     * This is where the values of the credentials are stored to create a connection to the database of the assignment.
     * @param url
     * @param username
     * @param password
     */
    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    /**
     * This function simply returns all the customers in the database, the customers are returned in an arraylist
     * while each customer is saved as a customer object from Customer model.
     * @return customer
     */
    @Override
    public List<Customer> findAll() {

        ArrayList<Customer> customer = new ArrayList<>();//Read all the database's customers
        String sql = "SELECT * FROM customer";

        try(
             Connection connection = DriverManager.getConnection(url, username, password)) {//Connecting to database
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet result = statement.executeQuery(); //Execute
            while(result.next()) {
                customer.add(new Customer(
                        result.getInt("customer_id"),
                                result.getString("first_name"),
                                result.getString("last_name"),
                                result.getString("country"),
                                result.getString("postal_code"),
                                result.getString("phone"),
                                result.getString("email")
                ));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customer;
    }

    /**
     * This function returns a specific customer given by the customer id, the function will return a customer object
     * created from the Customer model where details of the customer will be displayed from the database.
     * @param id
     * @return customer
     */
    @Override
    public Customer findById(Integer id) {
      Customer customer = null;
        String sql = "Select * FROM customer WHERE customer_id = ?";
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1,id);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                customer= new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customer;
    }


    /**
     * This
     * @param limit
     * @param offset
     * @return
     */
    @Override
    public List<Customer> findWithLimitAndOffset(int limit, int offset) {
        ArrayList<Customer> customer = new ArrayList<>();
        String sql = "SELECT * FROM customer LIMIT ? OFFSET ?";
        try(Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, limit);
            statement.setInt(2, offset);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                customer.add(new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                ));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customer;
    }

    /**
     * This function will return details of a specific customer given by the name, the function will return a customer object
     * created from the Customer model where the details will be stored.
     * @param firstName
     * @return customer
     */
    @Override
    public Customer findByName(String firstName) {

        Customer customer = null;

        String sql = "SELECT * FROM customer WHERE first_name LIKE ?";
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, "%"+ firstName +"%");
            ResultSet result = statement.executeQuery();

            while(result.next()) {
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    /**
     * This function allows the user to add a new customer in to the database, together with
     * information of the customer such as customer id, first name, last name, country, postal code
     * phone and email.
     * @param object
     * @return rowsaffected
     */
    @Override
    public int insert(Customer object) {

        String sql = "INSERT INTO customer (customer_id, first_name, last_name, country, postal_code, phone, email) VALUES(?,?,?,?,?,?,?)";
        int rowsaffected = 0;

        try(Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, object.id());
            statement.setString(2, object.firstName());
            statement.setString(3, object.lastName());
            statement.setString(4, object.country());
            statement.setString(5, object.postalCode());
            statement.setString(6, object.phoneNumber());
            statement.setString(7, object.email());
            rowsaffected = statement.executeUpdate();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return rowsaffected;
    }

    /**
     * This function allows the user to update an existing customer in the database, with information such as
     * first name, last name, country, postal code, phone and email of the new customer. The id of which customer
     * that will be updated also need to be given.
     * @param object
     * @return
     */
    @Override
    public int update(Customer object) {

        String sql = "UPDATE customer SET first_name = ?,last_name= ?,country= ?, postal_code= ?, phone= ?, email= ? WHERE customer_id =?";
        int rowsaffected = 0;

        try(Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, object.firstName());
            statement.setString(2, object.lastName());
            statement.setString(3, object.country());
            statement.setString(4, object.postalCode());
            statement.setString(5, object.phoneNumber());
            statement.setString(6, object.email());
            statement.setInt(7, object.id());
            rowsaffected = statement.executeUpdate();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return rowsaffected;
    }

    /**
     * This function returns the country with most customers in an object form of the CustomerCountry model.
     * @return country
     */
    @Override
    public CustomerCountry countryWithMostCustomers() {

        CustomerCountry country = null;

        String sql = "SELECT country FROM customer where country = (SELECT MAX(country) FROM customer) LIMIT 1";


        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()){
                country = new CustomerCountry(resultSet.getString("country"));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }


        return country;
    }

    /**
     * This function returns the customer who spends the most, the "customer" table has a one-to-many
     *  relation with the "invoice" table. Information will be fetched from both of these tables to find out
     *  the highest spender among the customer and the information of the customer (id, first name, last name and total invoice)
     *  will be returned in an object form of the CustomerSpender model.
     * @return highestSpender
     */
    @Override
    public ArrayList<CustomerSpender> highestSpender() {

        ArrayList<CustomerSpender> highestSpender= new ArrayList<>();

        String sql = "SELECT customer.customer_id, SUM(total) AS total, first_name, last_name,country,postal_code, phone, email\n" +
                "FROM invoice INNER JOIN customer ON invoice.customer_id=customer.customer_id \n" +
                "GROUP by customer.customer_id ORDER BY SUM(total) \n" +
                "DESC limit 1";

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()){
                highestSpender.add(new CustomerSpender(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getDouble("total")
                ));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }


        return highestSpender;
    }

    /**
     * With a given customer id, this function returns the most popular genre of the customer in the database.
     * The "customer" table has a one-to-many relation to "invoice" table, invoice table has a many-to-many relation to "track" table and
     * "track" table has a many-to-one relation to the "genre". We would see what genre that had the most tracks and from there, we would
     * see what customer is associated to invoices of those tracks.
     * @param id
     * @return mostPopularGenre
     */
    @Override
    public List<CustomerGenre> mostPopularGenre(int id) {

        ArrayList<CustomerGenre> mostPopularGenre= new ArrayList<>();

        String sql = "SELECT genre.name AS genre,COUNT(genre.genre_id) AS genre_id, customer.first_name AS first_name, customer.last_name AS last_name\n" +
                "FROM customer\n" +
                "INNER JOIN invoice ON customer.customer_id = invoice.customer_id\n" +
                "INNER JOIN invoice_line ON invoice_line.invoice_id = invoice.invoice_id\n" +
                "INNER JOIN track ON track.track_id = invoice_line.track_id\n" +
                "INNER JOIN genre ON genre.genre_id = track.genre_id\n" +
                "WHERE customer.customer_id = ?\n" +
                "GROUP BY genre.genre_id, customer.first_name, customer.last_name, genre.name\n" +
                "ORDER BY genre_id DESC\n" +
                "FETCH FIRST 1 ROWS WITH TIES";

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()){
                mostPopularGenre.add(new CustomerGenre(
                        resultSet.getString("genre"),
                        resultSet.getInt("genre_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name")
                ));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return mostPopularGenre;
    }

    @Override
    public int delete(Customer object) {
        return 0;
    }

    @Override
    public int deleteById(Customer object) {
        return 0;
    }
}
