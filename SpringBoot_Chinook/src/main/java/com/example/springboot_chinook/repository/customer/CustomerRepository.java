package com.example.springboot_chinook.repository.customer;

import com.example.springboot_chinook.models.*;
import com.example.springboot_chinook.repository.CrudRepository;

import java.util.List;

/**
 * This is the domain-specific repository, the CustomerRepository will be the child
 * repository extending the parent repository (CrudRepository).
 * The more specific methods that are used for assignment tasks are stored here.
 * The functions are overridden in the CustomerRepositoryImpl class, the logic of the methods will
 *  be further programmed in that class.
 */
public interface CustomerRepository extends CrudRepository<Customer, Integer> {

    List<Customer> findWithLimitAndOffset(int limit, int offset);

    Customer findByName(String firstName);

    CustomerCountry countryWithMostCustomers();

    List<CustomerSpender> highestSpender();

    List<CustomerGenre> mostPopularGenre(int id);

}
