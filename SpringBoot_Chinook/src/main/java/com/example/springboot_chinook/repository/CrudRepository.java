package com.example.springboot_chinook.repository;

import java.util.List;

/**
 * This is the generic repository, where the basic methods are stored to handle with JDBC
 * and fetch or manage with the data stored in the database. The CrudRepository will extend the CustomerRepository.
 * @param <T>
 * @param <ID>
 */
public interface CrudRepository<T, ID> {

    List<T> findAll();

    T findById(ID id);

    int insert(T object);

    int update(T object);

    int delete(T object);

    int deleteById(T object);
}
