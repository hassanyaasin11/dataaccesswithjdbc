# This assignment has two parts

 ### Appendix A: SQL scripts to create database.

Several scripts have been developed that may be executed to build a database, set up tables inside the database, add relationships between the tables, and populate the tables with data.
The database and its topic revolve around superhuman beings. The database may be referred to as SuperheroesDb.


### Appendix B: Reading data with JDBC

The second part of the project requires working with SQL data in Spring via JDBC and the PostgreSQL driver. A database is provided for this section of the assignment. It's commonly referred as as Chinook. First, we'll make a database in PgAdmin, then we'll launch a query tool, drop the script inside it, and finally, we'll put it to use. It needs to create the necessary tables and fill them in. Chinook is based on an analog of the iTunes customer-purchased-music database. With the help of Spring Boot, the right driver, and a custom-built repository pattern, we were able to successfully communicate with the backend database.

 ### Development environment.
 
#### These applications have been set up

• Intellij Ultimate <br />
• Postgres and PgAdmin <br />
• Java 17 <br />
• Postgres SQL driver dependency <br />

# Credits
#### Hassan Yaasin <br />
#### Hussen Ahmed 

# Licence
Copyright 2022, Noroff Accelerate AS




