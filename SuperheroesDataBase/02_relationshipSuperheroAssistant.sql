ALTER TABLE assistant
ADD superhero_id serial;

ALTER TABLE assistant
ADD CONSTRAINT fk_superhero
FOREIGN KEY (superhero_id) REFERENCES superhero(superhero_id);