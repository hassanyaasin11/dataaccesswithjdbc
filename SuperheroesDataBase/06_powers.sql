INSERT INTO power(power_name, power_description)
VALUES('Super strength', 'Enhanced strength ability, strength beyond normal human strength.'),
      ('Super speed', 'Enhanced speed, can move faster than normal human being.'),
      ('Flight', 'Can fly and move through air.'),
      ('Wall crawling', 'Can scale walls and ceilings, giving the ability to crawl upon vertical surfaces or up and down surfaces.');

INSERT INTO superhero_power_relationship(superhero_id, power_id)
VALUES(1,2),
      (2,1),
      (2,2),
      (2,3),
      (3,1),
      (3,4);