CREATE TABLE superhero_power_relationship (
    superhero_id serial NOT NULL,
    power_id serial NOT NULL,
    PRIMARY KEY(superhero_id, power_id)
);

ALTER TABLE superhero_power_relationship
    ADD CONSTRAINT fk_superhero_power_relationship
    FOREIGN KEY (superhero_id) REFERENCES superhero(superhero_id);

ALTER TABLE superhero_power_relationship
    ADD CONSTRAINT fk_superhero_power_relationship2
    FOREIGN KEY (power_id) REFERENCES power(power_id);