INSERT INTO superhero(superhero_name, superhero_alias, superhero_origin)
VALUES('Barry Allen', 'The Flash', 'Central City'),
      ('Clark Kent', 'Superman', 'Krypton'),
      ('Peter Parker', 'Spiderman', 'New York');